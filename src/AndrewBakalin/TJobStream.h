#ifndef _TJOBSTREAM_
#define _TJOBSTREAM_

#include "tproc.h"
#include "tqueue.h"
#include <iostream>

#define DefNumOfTacts 2000000

class TJobStream
{
private:
	int q1, ID; // �����������, ��� �������� ����� ������� � ����� �������� �������
	int JobsMissed; // ���������� ����������� ��-�� ������������ ������� �������
	int Tacts, IdleTacts; // ���������� ������� ������ � ������ �������
	TProc Proc;
	TQueue<int> Queue;

public:
	TJobStream(int _q1, int _q2, int QueueLen = MaxMemSize);
	void MakeTact(); // ��������� �� ����������� ��������� ��������� � ��������� ��������� ���� ����������
	void Start(int NumOfTacts = DefNumOfTacts); // ��������� ����� �����, �������� NumOfTacts ������
	void PrintStatictics(); // ������� ���������� ������ ����������
};

#endif